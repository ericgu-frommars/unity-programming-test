/// <summary>
/// Notification Center.
/// Copyright 2011 Stone Monkey Studios LLC
/// This Notification Center is an implementation of the Observer Pattern in C#.
/// To set up a listener call either:
/// NotificationCenter.defaultCenter.RegisterObserver (This will be called every time the specified notification is called, until removed.)
/// NotificationCenter.defaultCenter.RegisterObserverOnce (This will be called back once and then never again.)
/// 
/// To remove a listener, call:
/// NotificationCenter.defaultCenter.RemoveObserver (NOTE: You MUST remove an observer when you are done using it. This includes when an object is destroyed.
/// I recommend making a habit of Calling RemoveObserver from the OnDestroy or the OnDisable function of the observer script)
/// 
/// To Post a notification, call:
/// NotificationCenter.defaultCenter.PostNotification
/// 
/// For support, email:
/// support@stonemonkeystudios.com
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

/// <summary>
/// The notification class. This is what you will receive when you get a notification.
/// It contains a reference to the sender object, the notification name, and a Hashtable with any supplied data.
/// </summary>
public class Notification
{
	public object 			sender;
	public string			name;
	public Hashtable		data;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="Notification"/> class.
	/// </summary>
	/// <param name='sender'>
	/// Which object sent this message.
	/// </param>
	/// <param name='name'>
	/// The name of the notification.
	/// </param>
	/// <param name='data'>
	/// A hashtable will all the relevant data associated with the notification.
	/// </param>
	public Notification( object sender, string name, Hashtable data )
	{
		this.sender 	= sender;
		this.name		= name.ToLower();
		this.data		= data;
	}
}

public class RegisteredNotification
{
    public delegate void    notificationDelegate(Notification notif);
	public bool				once;
    public notificationDelegate callback;
	
	public RegisteredNotification(notificationDelegate callback, bool once)
	{
		this.callback   = callback;
		this.once	    = once;
	}
}

public class NotificationCenter
{
	static NotificationCenter m_defaultCenter;
	
	Dictionary<string, List<RegisteredNotification>> notifications;
	
	/// <summary>
	/// Gets the default center (A singleton for the notification center)
	/// </summary>
	/// <value>
	/// The default center.
	/// </value>
	public static NotificationCenter defaultCenter
	{
		get
		{
			if(m_defaultCenter != null)
				return m_defaultCenter;
			
			m_defaultCenter = new NotificationCenter();
			return m_defaultCenter;
		}
	}
	
	/// <summary>
	/// Registers the observer.
	/// </summary>
	/// <param name='comp'>
	/// Specific component to notify. Usually "this";
	/// </param>
	/// <param name='notificationName'>
	/// Notification name to wait for.
	/// </param>
	/// <param name='method'>
	/// Method to call when notification is posted. This method should take 1 parameter, representing the GameObject posting the notification.
	/// </param>
	public void RegisterObserver(string notificationName, RegisteredNotification.notificationDelegate callback)
	{
		if(notifications == null)
			notifications = new Dictionary<string, List<RegisteredNotification>>();
		
		List<RegisteredNotification> list;
		if(!notifications.TryGetValue(notificationName.ToLower(), out list))
		{	
			list = new List<RegisteredNotification>();
			notifications.Add(notificationName.ToLower(), list);
		}
		
		if(list != null)
			list.Add(new RegisteredNotification(callback, false));
		
	}
	
	/// <summary>
	/// Registers the observer and will remove observer after one notification
	/// </summary>
	/// <param name='comp'>
	/// Specific component to notify. Usually "this";
	/// </param>
	/// <param name='notificationName'>
	/// Notification name to wait for.
	/// </param>
	/// <param name='method'>
	/// Method to call when notification is posted. This method should take 1 parameter, representing the GameObject posting the notification.
	/// </param>
	public void RegisterObserverOnce(string notificationName, RegisteredNotification.notificationDelegate callback)
	{
		if(notifications == null)
			notifications = new Dictionary<string, List<RegisteredNotification>>();
		
		List<RegisteredNotification> list;
		if(!notifications.TryGetValue(notificationName.ToLower(), out list))
		{	
			list = new List<RegisteredNotification>();
			notifications.Add(notificationName.ToLower(), list);
		}
		
		if(list != null)
			list.Add(new RegisteredNotification(callback, true));
	}
	
	/// <summary>
	/// Removes the specified observer.
	/// </summary>
	/// <returns>
	/// <c>True</c> if removed successfully.
	/// <c>False</c> if the specified observer does not exist.
	/// </returns>
	/// <param name='comp'>
	/// Specific component to remove as observer, usually 'this'.
	/// </param>
	/// <param name='notificationName'>
	/// Name of notification.
	/// </param>
	public bool RemoveObserver(string notificationName, RegisteredNotification.notificationDelegate callback)
	{	
		if(notificationName == "" || callback == null || notifications == null)
			return false;
		
		List<RegisteredNotification> list;
		if(notifications.TryGetValue(notificationName.ToLower(), out list))
		{
			int i = 0;
			int count = list.Count;
			while(i < count)
			{
                if(list[i].callback == callback)
				{
                    list[i].callback = null;
					list.RemoveAt(i);
					count--;
					continue;
				}
				i++;
			}
			if(count == list.Count)
				return false;
			
			return true;
		}
		return false;
	}
	
	/// <summary>
	/// Posts the notification to any observers.
	/// </summary>
	/// <param name='sender'>
	/// The object that is posting usually 'this'.
	/// </param>
	/// <param name='notificationName'>
	/// Notification name posting.
	/// </param>
	/// <param name='parameters'>
	/// A list of additional parameters to pass with the notification. Note: This is wrapped in the Notification class,
	/// so the only parameter of your observing function should be a Notification object.
	/// </param>
	public void PostNotification(object sender, string notificationName, Hashtable data )
	{
		if(notifications == null || notificationName == "")
		{
			return;
		}
		
		List<RegisteredNotification> list;
		
		if(!notifications.TryGetValue(notificationName.ToLower(), out list))
			return;
		
		if(list == null)
			return;

		int i 		= 0;
		int count	= list.Count;
		
		
		
		while(i < count)
		{
			RegisteredNotification reg = list[i];
			
			
			Notification notif = new Notification(sender, notificationName.ToLower(), data);
            RegisteredNotification.notificationDelegate callbackCopy = reg.callback;
			
			bool removed = false;
			if(reg.once)
			{
				list.RemoveAt(i);
				count--;
				removed = true;
			}
			
			callbackCopy(notif);
			
			if(!removed)
				i++;
		}
	}
}
